package com.example.dummyuserlisting.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.dummyuserlisting.data.entities.User
import com.example.dummyuserlisting.data.entities.UserProps
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable


@Dao
interface UserDao{

    @Query("SELECT * FROM user_table")
    fun getUserList(): Observable<List<UserProps>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUsers(userProps: List<UserProps>)


    @Query("UPDATE user_table SET user_accept = :accept  WHERE user_email = :id")
    fun updateUserAcceptStatus(id: String, accept: Boolean)

    @Query("UPDATE user_table SET user_reject = :reject WHERE user_email = :id")
    fun updateUserDeclineStatus(id: String, reject: Boolean)

    @Query("DELETE FROM user_table")
    fun clearTable()

}