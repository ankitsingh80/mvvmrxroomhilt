package com.example.dummyuserlisting.data.repo

import com.example.dummyuserlisting.data.local.UserDao
import com.example.dummyuserlisting.data.entities.UserProps
import com.example.dummyuserlisting.data.remote.UserRemoteDataSource
import com.example.dummyuserlisting.di.MainThreadScheduler
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class UserRepository @Inject constructor(private val remoteDataSource: UserRemoteDataSource,
                                         private val localDataSource: UserDao,
                                         @MainThreadScheduler private val bgThreadScheduler: Scheduler
) {

    fun getUsersList(): Observable<List<UserProps>> {
        return loadFromDb()
    }

    fun saveAcceptStatus(id: String) {
        GlobalScope.launch {
            localDataSource.updateUserAcceptStatus(id,true)
        }

    }

    fun saveDeclineStatus(id: String) {
        GlobalScope.launch {
            localDataSource.updateUserDeclineStatus(id, true)
        }
    }

    private fun loadFromDb(): Observable<List<UserProps>> = localDataSource.getUserList().flatMap {
        if(it.isEmpty().not()){
            Observable.just(it)
        }else{
            fetchFromNetwork()
        }
    }

    private fun fetchFromNetwork(): Observable<List<UserProps>> {
        return remoteDataSource.getUsers()
            .subscribeOn(bgThreadScheduler)
            .map { it ->
                it.results.map {
                    UserProps(email = it.email,accept = false, reject = false)
                }
            }.doOnNext {
                insertList(it)
            }
    }

    private fun insertList(userProps: List<UserProps>) {
        localDataSource.clearTable()
        localDataSource.insertAllUsers(userProps)
    }

}

