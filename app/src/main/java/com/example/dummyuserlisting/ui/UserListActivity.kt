package com.example.dummyuserlisting.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dummyuserlisting.R
import com.example.dummyuserlisting.di.MainThreadScheduler
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@AndroidEntryPoint
class UserListActivity : AppCompatActivity(), UserListAdapter.OnItemClickListener {

    private val userListViewModel: UserListViewModel by viewModels()
    private lateinit var userListAdapter : UserListAdapter

    @Inject
    lateinit var compositeDisposable :CompositeDisposable

    @MainThreadScheduler
    @Inject
    lateinit var  mainThreadScheduler:Scheduler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setAdapter()
        requestData()
        observeProgressBar()
        observeUsersData()
    }

    private fun requestData() {
        compositeDisposable.add(userListViewModel.getUserData())
    }

    private fun setAdapter() {
        userListAdapter = UserListAdapter()
        rvUserList.adapter = userListAdapter
        rvUserList.layoutManager = LinearLayoutManager(this)
        userListAdapter.setOnItemClickListener(this)
    }

    private fun observeProgressBar() {
        compositeDisposable.add(userListViewModel.observeProgressBar()
            .observeOn(mainThreadScheduler)
            .subscribe {
                if (it) {
                    showProgressBar()
                } else
                    hideProgressBar()
            })
    }

    private fun observeUsersData() {
        compositeDisposable.add(userListViewModel.observeUserData()
            .observeOn(mainThreadScheduler)
            .subscribe {
                userListAdapter.submitList(it)
                progressBar.visibility = View.GONE
            })
    }

    override fun onAcceptClick(id:String) {
        userListViewModel.saveAcceptStatus(id)
    }

    override fun onDeclineClick(id:String) {
        userListViewModel.saveDeclineStatus(id)
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        userListViewModel.onClear()
        if(compositeDisposable.isDisposed.not())
            compositeDisposable.dispose()
    }


}